$(function () {
    addFileChangeEventListener();
    addUploadSuccessEventListener();
    addTalkEventListener();
    initLayerPhoto();
});
//加载代码高亮js
loadscript("/templates/jpress-perfree-simple/static/plugin/highlight/highlight.pack.js", function () {
    hljs.initHighlighting();
});


/**
* @Author Frank
* @Description 加载layer-相册插件
* @Date Create in  2019-05-14 14:20
* @param
* @return
*/
function initLayerPhoto(){
    layer.photos({
        photos: '.talk-history-body-content'
        , anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
    });
}
//加载js
function loadscript(url, callback) {
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState) {
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {
        script.onload = function () {
            callback();
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}



Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "H+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


function show() {
    layer.open({
        type: 1,
        title: '插入图片',
        skin: 'layui-layer-rim', //加上边框
        area: ['400px', '350px'], //宽高
        content: $('.layer-upload')
    });
    $("#time-upload").bind("click", function () {
        $("#time_file").trigger("click");

    });

}


/**
* @Author Frank
* @Description 绑定上传成功确定按钮
* @Date Create in  2019-05-14 14:18
* @param
* @return
*/
function addUploadSuccessEventListener() {
    $("#imageInsertOk").bind("click", function () {
        var str = $("input[ name='imageInsertModal']").val();
        console.log(str);
        layer.closeAll();
        var imgArray = str.split(",");
        for (var i = 0; i < imgArray.length; i++) {
            var aStr = "";
            if (i == 0) {
                aStr += "<br>";
            }
            aStr += "<a class='aImg'> " +
                "<img src=" + imgArray[i] + "> </a>";
            var ele = document.getElementById("textarea-content");
            ele.value = ele.value + aStr;
        }
    });
}

/**
 * @Author Frank
 * @Description 绑定监听文件上传input
 * @Date Create in  2019-05-14 14:11
 * @param
 * @return
 */
function addFileChangeEventListener() {
    $("#time_file").bind("change", function () {
        if (!$(this).val()) {
            $("#file-info").html("没有选择文件");
            return;
        }
        var input = $('#time_file');
        // 相当于： $input[0].files, $input.get(0).files
        var files = input.prop('files');
        console.log(files);
        var formData = new FormData();
        for (var i = 0; i < files.length; i++) {
            var file=files[i];
            if (file.type!=="image/jpeg" && file.type!=="image/png" && file.type!=="image/gif"){
                $("#file-info").val("错误的文件类型,只能选择图片" + file.type);
                layer.msg("错误的文件类型,只能选择图片");
                return;
            }
            formData.append('file', files[i]);  //添加图片信息的参数
        }
        $.ajax({
            url: '/talk/postTalkUpload',
            type: 'POST',
            cache: false, //上传文件不需要缓存
            data: formData,
            processData: false, // 告诉jQuery不要去处理发送的数据
            contentType: false, // 告诉jQuery不要去设置Content-Type请求头
            success: function (data) {
                if (data.success) {
                    if (data != undefined && data != "" && data.list.length > 0) {
                        var str = "";
                        for (var i = 0; i < data.list.length; i++) {
                            str += data.list[i] + ",";
                            $("#time-upload").text("选择文件");
                            $("#time-upload").attr("disabled", false);
                        }
                        str = str.substring(0, str.length - 1);
                        $("input[ name='imageInsertModal']").val(str);//插入返回的图片地址
                    }
                } else {
                    layer.msg(data.message);
                }
            },
            error: function (data) {
                $("#time-upload").attr("disabled", false);
                $("#time-upload").text("选择文件");
                $("#file-info").val($("#file-info").val() + "上传失败" + data);
            }
        })
        $("#file-info").val(files[0].name);
        $("#time-upload").text("正在上传");
        $("#time-upload").attr("disabled", true);
    })
}

/**
 * @Author Frank
 * @Description 监听发表新鲜事按钮
 * @Date Create in  2019-05-14 10:21
 * @param
 * @return
 */
function addTalkEventListener() {
    $(".btn-success").bind("click", function () {
        var strContent = document.getElementById("textarea-content").value;
        if (strContent != undefined && strContent != "") {
            console.log("处理前的strContent为\r\n" + strContent);
            strContent = strContent.replace(/\r\n/g, '<br/>'); //IE9、FF、chrome
            strContent = strContent.replace(/\n/g, '<br/>'); //IE7-8
            /*strContent = strContent.replace(/\s/g, ' '); //空格处理*/
            console.log("转换之后的html代码为\r\n" + strContent);
            /* document.getElementById("show").innerHTML = strContent;*/
            $.ajax({
                url: '/talk/talkPost',
                type: 'POST',
                data: {"content": strContent},
                success: function (data) {
                    if (data.success) {
                        $("#file-info").val("");
                        layer.msg('发表成功');
                        var time2 = new Date().Format("yyyy-MM-dd HH:mm:ss");
                        $(".talk-history-all").prepend("<div class=\"talk-history\"> \n" +
                            "           <a class=\"talk-history-headImg\"> <img src=\"/attachment/20190426/8ccf48a855b345eca62f0dca273f6ff8.JPG\" class=\"avatar-40 photo img-circle\" style=\"\"> </a> \n" +
                            "           <div class=\"talk-history-body\"> \n" +
                            "            <div class=\"talk-history-body-right\"> \n" +
                            "             <div class=\"talk-history-body-head\"> \n" +
                            "              <span class=\"arrow left\"></span> \n" +
                            "              <p class=\"body-head-name\"> Frank</p> \n" +
                            "              <span class=\"body-head-time\">" + time2 + "</span> \n" +
                            "             </div> \n" +
                            "             <div class=\"talk-history-body-content\">\n" + strContent +
                            "              </div> \n" +
                            "             <div class=\"talk-history-body-bottom\"> \n" +
                            "              <a> <i class=\"fa fa-heart-o\" aria-hidden=\"true\"> 0</i> </a> \n" +
                            "              <a> <i class=\"fa fa-dot-circle-o\" aria-hidden=\"true\"> 发自网页 </i> </a> \n" +
                            "             </div> \n" +
                            "            </div> \n" +
                            "           </div> \n" +
                            "          </div>");
                        layer.photos({
                            photos: '.talk-history-body-content'
                            , anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
                        });
                        var obj = document.getElementById("time_file");
                        obj.select();
                        $(':input', '#talk-publish-form')
                            .not(':button, :submit, :reset, :hidden')
                            .val('')
                            .removeAttr('checked')
                            .removeAttr('selected');
                        $("input[ name='imageInsertModal']").val("");
                    } else {
                        layer.msg(data.message);
                    }
                },
                error: function (data) {

                }
            })
        } else {
            layer.msg("发表内容不能为空")
            return;
        }

    });
}

$(".like-num").on("click", function () {
    console.log($(this).attr("id"));
    $.ajax({
        url: '/talk/talkPost',
        type: 'POST',
        data: {"content": strContent},
        success: function (data) {
            if (data.success) {

            } else {
                layer.msg(data.message);
            }
        },
        error: function () {

        }
    })
})
