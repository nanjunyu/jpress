$(function () {
    initLayerPhoto();
});
//加载代码高亮js
loadscript("/templates/jpress-perfree-simple/static/plugin/highlight/highlight.pack.js", function () {
    hljs.initHighlighting();
});


/**
* @Author Frank
* @Description 加载layer-相册插件
* @Date Create in  2019-05-14 14:20
* @param
* @return
*/
function initLayerPhoto(){
    layer.photos({
        photos: '.baguetteBoxOne'
        , anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
    });
}
//加载js
function loadscript(url, callback) {
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState) {
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {
        script.onload = function () {
            callback();
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}


