package io.jpress.web.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.weixin.sdk.api.ApiResult;
import io.jboot.utils.StrUtil;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.JPressConsts;
import io.jpress.JPressOptions;
import io.jpress.commons.utils.CheckUtil;
import io.jpress.commons.utils.DateUtils;
import io.jpress.commons.utils.SHA1;
import io.jpress.web.base.ControllerBase;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.*;

import static io.jboot.utils.HttpUtil.httpGet;

/**
 * jsSdk相关的api
 *
 * @Author Frank
 * @Description
 * @Date: Create in  2019-07-11 10:16
 */
@RequestMapping("/weChat/jsSdk")
public class WeChatJsSDkController extends ControllerBase {

    public static String jsApiTicket = null;
    public static String token = null;
    private final static Logger logger = LoggerFactory.getLogger(WeChatJsSDkController.class);


    public void config() {
        Map<Object, Object> retMap = new HashMap<>(16);
        String url = getPara("url");
        logger.info("url"+url);
        if (!StringUtils.isEmpty(url)) {
            try {
                url = java.net.URLDecoder.decode(url, "UTF-8");
                logger.info("URLDecoder---url"+url);
                if (!StringUtils.isEmpty(jsApiTicket)) {
                    if (StringUtils.isEmpty(token)) {
                        token = getToken();
                    }
                    retMap = JsSdkConfig(url, jsApiTicket);
                } else {
                    if (!StringUtils.isEmpty(token)) {
                        jsApiTicket = getJsApiTicket(token);
                    } else {
                        token = getToken();
                        jsApiTicket = getJsApiTicket(token);
                    }
                    retMap = JsSdkConfig(url, jsApiTicket);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        renderText(JSON.toJSONString(retMap));
    }

   /* public class ScheduledExecutorServiceTest {
        public static void main(String[] args) {
            ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
            executorService.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    System.out.println("run " + System.currentTimeMillis());
                }
            }, 0, 2, TimeUnit.HOURS);
        }
    }*/


    /**
     * @param： HtmlPath: 需要注入权限的页面完整路径需要带参数
     * @param: jsApiTicket 微信jsApiTicket 通过token获取
     * @return： Map:  config接口注入权限验证配置需要的参数
     * @Description： 微信js-sdk  通过config接口注入权限验证配置
     * @Author： NanJunYu
     * @Date: 2016/10/31  15:15
     */
    public static Map<Object, Object> JsSdkConfig(String HtmlPath, String jsApiTicket) throws UnsupportedEncodingException {
        SortedMap<Object, Object> configMap = new TreeMap<>();
        //String nonceStr = DateUtils.getCurrTime() + DateUtils.getNumberString(10);

        String noncestr =getRandomStringByLength(32);
        long timestamp = new Date().getTime()/1000;

       // String timestamp = DateUtils.getTimeStamp();
        configMap.put("noncestr", noncestr);
        configMap.put("jsapi_ticket", jsApiTicket);
        configMap.put("timestamp", timestamp);
        configMap.put("url", HtmlPath);
        logger.info("noncestr:"+noncestr+"\n"+"jsapi_ticket:"+jsApiTicket+"\n"+"timestamp:"+timestamp+"\n"+"url:"+HtmlPath);

        String sign =  "jsapi_ticket="+jsApiTicket+"&noncestr=" +noncestr+"&timestamp="+timestamp +"&url="+HtmlPath;
        logger.info("sign:"+sign);

        String signature = new SHA1().getDigestOfString(sign.getBytes("utf-8"));

        logger.info("signature:"+signature);


        //SHA1加密签名
       // String signature = createSign_wx_config("UTF-8", configMap);
        String appId = JPressOptions.get(JPressConsts.OPTION_WECHAT_APPID);
        configMap.put("appId", appId);
        configMap.put("signature", signature);
        return configMap;
    }


    public static String getRandomStringByLength(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }


    public static String createSign_wx_config(String characterEncoding, SortedMap<Object, Object> parameters) {
        StringBuffer sb = new StringBuffer();
        Set<Map.Entry<Object, Object>> es = parameters.entrySet();
        Iterator<Map.Entry<Object, Object>> it = es.iterator();
        while (it.hasNext()) {
            Map.Entry<Object, Object> entry = (Map.Entry<Object, Object>) it.next();
            String k = (String) entry.getKey();
            Object v = entry.getValue();
            if (null != v && !"".equals(v)) {
                sb.append(k + "=" + v + "&");
            }
        }
        String str = sb.toString();
        String sign = CheckUtil.getSha1(str.substring(0, str.length() - 1));
        return sign;
    }

    protected String getJsApiTicket(String token) {
        logger.info("getJsApiTicket---before----token="+token);
        String jsApiTicket=null;
        StringBuilder sb = new StringBuilder("https://api.weixin.qq.com/cgi-bin/ticket/getticket?");
        sb.append("access_token=" + token);
        sb.append("&type=jsApi");
        String httpString = httpGet(sb.toString());
        logger.info("getJsApiTicket--------------结果"+httpString);
        if (!StrUtil.isBlank(httpString)) {
            jsApiTicket=JSON.parseObject(httpString).get("ticket").toString();
            return jsApiTicket;
        }
        return jsApiTicket;
    }

    protected String getToken() {
        String token = null;
        StringBuilder sb = new StringBuilder("https://api.weixin.qq.com/cgi-bin/token");
        sb.append("?grant_type=client_credential");
        String appId = JPressOptions.get(JPressConsts.OPTION_WECHAT_APPID);
        logger.info("appId"+appId);
        String secret = JPressOptions.get(JPressConsts.OPTION_WECHAT_APPSECRET);
        logger.info("secret"+secret);
        sb.append("&appid=" + appId);
        sb.append("&secret=" + secret);
        String httpString = httpGet(sb.toString());
        logger.info("getToken--------------结果"+httpString);
        if (!StrUtil.isBlank(httpString)) {
            JSONObject object = JSON.parseObject(httpString);
            token = object.get("access_token").toString();
        }
        return token;
    }
}
