package io.jpress.module.photo.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.service.JbootServiceJoiner;
import io.jpress.module.photo.model.UserPhotoAlbum;

import java.util.List;

public interface UserPhotoAlbumService extends JbootServiceJoiner {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public UserPhotoAlbum findById(Object id);


    /**
     * find all model
     *
     * @return all <UserPhotoAlbum
     */
    public List<UserPhotoAlbum> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(UserPhotoAlbum model);


    /**
     * save model to database
     *
     * @param model
     * @return  id value if save success
     */
    public Object save(UserPhotoAlbum model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if saveOrUpdate success
     */
    public Object saveOrUpdate(UserPhotoAlbum model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(UserPhotoAlbum model);


    /**
     * paginate query
     *
     * @param page
     * @param pageSize
     * @return
     */
    public Page<UserPhotoAlbum> paginate(int page, int pageSize);


}