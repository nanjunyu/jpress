package io.jpress.module.photo.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.service.JbootServiceJoiner;
import io.jpress.module.photo.model.UserPhoto;

import java.util.List;

public interface UserPhotoService extends JbootServiceJoiner {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public UserPhoto findById(Object id);


    /**
     * find all model
     *
     * @return all <UserPhoto
     */
    public List<UserPhoto> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(UserPhoto model);


    /**
     * save model to database
     *
     * @param model
     * @return  id value if save success
     */
    public Object save(UserPhoto model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if saveOrUpdate success
     */
    public Object saveOrUpdate(UserPhoto model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(UserPhoto model);


    /**
     * paginate query
     *
     * @param page
     * @param pageSize
     * @return
     */
    public Page<UserPhoto> paginate(int page, int pageSize);

    public Page<UserPhoto> _paginateByAlbumId(int page, int pagesize, Long albumId);

    public List<UserPhoto> getAllByAlbumId(Long albumId);


}
