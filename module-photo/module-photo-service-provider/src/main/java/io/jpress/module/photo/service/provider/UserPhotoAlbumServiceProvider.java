package io.jpress.module.photo.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.db.model.Columns;
import io.jpress.module.photo.service.UserPhotoAlbumService;
import io.jpress.module.photo.model.UserPhotoAlbum;
import io.jboot.service.JbootServiceBase;

import java.util.List;

@Bean
public class UserPhotoAlbumServiceProvider extends JbootServiceBase<UserPhotoAlbum> implements UserPhotoAlbumService {

    @Override
    public List<UserPhotoAlbum> findAll() {
        Columns columns = Columns.create("status", "1");
        List<UserPhotoAlbum> userPhotoList = DAO.findListByColumns(columns);
        return userPhotoList;
    }
}
