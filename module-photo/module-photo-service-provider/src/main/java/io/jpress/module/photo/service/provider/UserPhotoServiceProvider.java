package io.jpress.module.photo.service.provider;

import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.aop.annotation.Bean;
import io.jboot.db.model.Column;
import io.jboot.db.model.Columns;
import io.jpress.module.photo.service.UserPhotoService;
import io.jpress.module.photo.model.UserPhoto;
import io.jboot.service.JbootServiceBase;

import java.util.List;

@Bean
public class UserPhotoServiceProvider extends JbootServiceBase<UserPhoto> implements UserPhotoService {
    @Inject
    UserPhotoService userPhotoService;

    @Override
    public Page<UserPhoto> _paginateByAlbumId(int page, int pagesize, Long albumId) {
        Columns columns = Columns.create("photo_album_id", albumId);
        Page<UserPhoto> p = DAO.paginateByColumns(
                page,
                pagesize,
                columns,
                "created desc");


        userPhotoService.join(p, "photo_album_id");
        return p;
    }

    @Override
    public List<UserPhoto> getAllByAlbumId(Long albumId) {
        Columns columns = Columns.create("photo_album_id", albumId);
        List<UserPhoto> userPhotoList = DAO.findListByColumns(columns);
        return userPhotoList;
    }
}
