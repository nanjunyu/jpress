package io.jpress.module.photo.controller;

import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import io.jpress.core.template.Template;
import io.jpress.module.photo.model.UserPhotoAlbum;
import io.jpress.module.photo.service.UserPhotoAlbumService;
import io.jpress.web.base.TemplateControllerBase;

import java.util.List;

/**
 * @Author Frank
 * @Date Create in  2019/4/30 13:59
 */
@RequestMapping("/photoAlbum")
public class PhotoAlbumController extends TemplateControllerBase {


    @Inject
    UserPhotoAlbumService userPhotoAlbumService;


   public  void index(){
       render("page_album_list.html");
   }
}
