package io.jpress.module.photo.controller;

import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.directive.base.JbootDirectiveBase;
import io.jpress.module.photo.service.UserPhotoService;
import io.jpress.web.base.TemplateControllerBase;

/**
 * @Author Frank
 * @Date Create in  2019/4/30 13:59
 */
@RequestMapping("/photoMe")
public class PhotoController extends TemplateControllerBase {
    @Inject
    UserPhotoService userPhotoService;


    public void index() {
       /* Long albumId = getParaToLong("albumId");*/
        Long albumId = getParaToLong(0);
        setAttr("albumId", albumId);
        render("page_photo_list.html");
    }
}
