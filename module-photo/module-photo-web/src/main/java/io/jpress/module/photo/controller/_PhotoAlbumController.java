package io.jpress.module.photo.controller;

import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.validate.EmptyValidate;
import io.jboot.web.validate.Form;
import io.jpress.JPressConsts;
import io.jpress.core.menu.annotation.AdminMenu;
import io.jpress.module.photo.model.UserPhotoAlbum;
import io.jpress.module.photo.service.UserPhotoAlbumService;
import io.jpress.web.base.AdminControllerBase;

import java.util.List;

/**
 * @Author Frank
 * @Date Create in  2019/4/30 14:34
 */
@RequestMapping(value="/admin/photoAlbum", viewPath = JPressConsts.DEFAULT_ADMIN_VIEW)

public class _PhotoAlbumController extends AdminControllerBase {
    @Inject
    UserPhotoAlbumService userPhotoAlbumService;

    @AdminMenu(groupId = "photo",text = "相册列表",order = 1)
    public void index(){
        Page<UserPhotoAlbum> photoAlbumPage= userPhotoAlbumService.paginate(getPagePara(),10);
        setAttr("page", photoAlbumPage);
        render("photo/list.html");
    }

    @AdminMenu(text = "上传照片获取所有相册信息", groupId = JPressConsts.SYSTEM_MENU_PHOTO, order = 3)
    public void toUpload() {
        List<UserPhotoAlbum> photoAlbumList = userPhotoAlbumService.findAll();
        setAttr("photoAlbumList", photoAlbumList);
        render("photo/upload.html");
    }


    @AdminMenu(text = "创建相册", groupId = JPressConsts.SYSTEM_MENU_PHOTO, order = 2)
    public void upload()
    {
        render("photo/album.html");
    }


    @EmptyValidate({
            @Form(name = "photoAlbum.title", message = "相册名称不能为空"),
    })
    public void doAlbumSave() {
        UserPhotoAlbum userPhotoAlbum = getModel(UserPhotoAlbum.class, "photoAlbum");
        saveAlbum(userPhotoAlbum);
    }

    private void saveAlbum(UserPhotoAlbum userPhotoAlbum) {
        userPhotoAlbum.setUserId(getLoginedUser().getId().intValue());
        userPhotoAlbumService.save(userPhotoAlbum);
        renderOkJson();
    }


}
