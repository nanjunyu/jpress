package io.jpress.module.photo.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;
import io.jboot.utils.FileUtil;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.JPressConsts;
import io.jpress.JPressOptions;
import io.jpress.commons.utils.AliyunOssUtils;
import io.jpress.commons.utils.AttachmentUtils;
import io.jpress.commons.utils.PhotoUtils;
import io.jpress.module.photo.model.UserPhoto;
import io.jpress.module.photo.model.UserPhotoAlbum;
import io.jpress.module.photo.service.UserPhotoAlbumService;
import io.jpress.module.photo.service.UserPhotoService;
import io.jpress.web.base.AdminControllerBase;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

/**
 * @Author Frank
 * @Date Create in  2019/4/29 16:08
 */
@RequestMapping(value = "/admin/photo", viewPath = JPressConsts.DEFAULT_ADMIN_VIEW)
public class _PhotoController extends AdminControllerBase {

    @Inject
    UserPhotoService userPhotoService;

    @Inject
    UserPhotoAlbumService userPhotoAlbumService;



    /**
    *   根据相册id获取所有照片
    *
    * @Author Frank
    * @Date Create in  2019/4/30 17:25
    * @param
    * @return
    */
    public void albumDetail() {
        Long photoAlbumId = getParaToLong();
        Page<UserPhoto> photoAlbumPage= userPhotoService._paginateByAlbumId(getPagePara(),30,photoAlbumId);
        setAttr("page", photoAlbumPage);
        render("photo/detail.html");
    }

    public void upload() {
        if (!isMultipartRequest()) {
            renderError(404);
            return;
        }

        UploadFile uploadFile = getFile();
        if (uploadFile == null) {
            renderJson(Ret.fail().set("message", "请选择要上传的文件"));
            return;
        }

        File file = uploadFile.getFile();
        if (PhotoUtils.isUnSafe(file)){
            file.delete();
            renderJson(Ret.fail().set("message", "不支持此类文件上传"));
            return;
        }

        String mineType = uploadFile.getContentType();
        String fileType = mineType.split("/")[0];
        Integer maxImgSize = JPressOptions.getAsInt("attachment_img_maxsize", 10);
        Integer maxOtherSize = JPressOptions.getAsInt("attachment_other_maxsize", 100);
        Integer maxSize = "image".equals(fileType) ? maxImgSize : maxOtherSize;

        int fileSize = Math.round(file.length() / 1024 * 100) / 100;
        if (fileSize > maxSize * 1024) {
            file.delete();
            renderJson(Ret.fail().set("message", "上传文件大小不能超过 " + maxSize + " MB"));
            return;
        }

        String path = PhotoUtils.moveFile(uploadFile,"photo");
        AliyunOssUtils.upload(path, PhotoUtils.file(path));

        UserPhoto userPhoto = new UserPhoto();
        Long photoAlbumId = getParaToLong("photoAlbumId");
        userPhoto.setPhotoAlbumId(photoAlbumId);
        userPhoto.setUserId(getLoginedUser().getId());
        userPhoto.setTitle(uploadFile.getOriginalFileName());
        String dbPath=path.replace("\\", "/");
        userPhoto.setPath(dbPath);
        userPhoto.setSuffix(FileUtil.getSuffix(uploadFile.getFileName()));
        userPhoto.setMimeType(uploadFile.getContentType());

        userPhotoService.save(userPhoto);
        UserPhotoAlbum userPhotoAlbum=userPhotoAlbumService.findById(photoAlbumId);
        if(StringUtils.isEmpty(userPhotoAlbum.getThumbnail())){
            userPhotoAlbum.setThumbnail(dbPath);
            userPhotoAlbumService.update(userPhotoAlbum);
        }
        renderJson(Ret.ok().set("success", true).set("src", userPhoto.getPath()));
    }


}
