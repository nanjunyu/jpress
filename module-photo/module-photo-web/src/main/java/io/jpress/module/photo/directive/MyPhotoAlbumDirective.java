package io.jpress.module.photo.directive;

import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import io.jpress.module.photo.model.UserPhotoAlbum;
import io.jpress.module.photo.service.UserPhotoAlbumService;

import java.util.List;

/**
 * @Author Frank
 * @Description
 * @Date: Create in  2019-05-06 14:02
 */
@JFinalDirective("myAlbumPhoto")
public class MyPhotoAlbumDirective extends JbootDirectiveBase {
    @Inject
    UserPhotoAlbumService userPhotoAlbumService;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        List<UserPhotoAlbum> userPhotoAlbum = userPhotoAlbumService.findAll();
        scope.setLocal("photoAlbumList", userPhotoAlbum);
        renderBody(env,scope,writer);
    }

    @Override
    public boolean hasEnd(){
        return true;
    }
}
