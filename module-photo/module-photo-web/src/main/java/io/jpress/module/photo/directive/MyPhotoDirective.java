package io.jpress.module.photo.directive;

import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import io.jpress.module.photo.model.UserPhoto;
import io.jpress.module.photo.model.UserPhotoAlbum;
import io.jpress.module.photo.service.UserPhotoAlbumService;
import io.jpress.module.photo.service.UserPhotoService;
import sun.rmi.runtime.Log;

import java.util.List;

/**
 * @Author Frank
 * @Description
 * @Date: Create in  2019-05-06 14:02
 */
@JFinalDirective("myPhoto")
public class MyPhotoDirective extends JbootDirectiveBase {
    @Inject
    UserPhotoService userPhotoService;

    @Inject
    UserPhotoAlbumService userPhotoAlbumService;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
       /* Long albumId=getParaToLong("albumId",scope);*/
        Long albumId = getPara(0, scope);
        UserPhotoAlbum userPhotoAlbum= userPhotoAlbumService.findById(albumId);
        List<UserPhoto> userPhotoList = userPhotoService.getAllByAlbumId(albumId);
        scope.setLocal("photoAlbumName", userPhotoAlbum.getTitle());
        scope.setLocal("photoAlbumSize", userPhotoList.size());
        scope.setLocal("photoList", userPhotoList);
        renderBody(env,scope,writer);
    }

    @Override
    public boolean hasEnd(){
        return true;
    }
}
