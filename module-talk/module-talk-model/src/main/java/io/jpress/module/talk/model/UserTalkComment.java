package io.jpress.module.talk.model;

import io.jboot.db.annotation.Table;
import io.jpress.module.talk.model.base.BaseUserTalkComment;

/**
 * Generated by Jboot.
 */
@Table(tableName = "user_talk_comment", primaryKey = "id")
public class UserTalkComment extends BaseUserTalkComment<UserTalkComment> {
	
}
