package io.jpress.module.talk.service;

import com.jfinal.plugin.activerecord.Page;
import io.jpress.module.talk.model.UserTalkComment;

import java.util.List;

public interface UserTalkCommentService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public UserTalkComment findById(Object id);


    /**
     * find all model
     *
     * @return all <UserTalkComment
     */
    public List<UserTalkComment> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(UserTalkComment model);


    /**
     * save model to database
     *
     * @param model
     * @return  id value if save success
     */
    public Object save(UserTalkComment model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if saveOrUpdate success
     */
    public Object saveOrUpdate(UserTalkComment model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(UserTalkComment model);


    /**
     * paginate query
     *
     * @param page
     * @param pageSize
     * @return
     */
    public Page<UserTalkComment> paginate(int page, int pageSize);


}