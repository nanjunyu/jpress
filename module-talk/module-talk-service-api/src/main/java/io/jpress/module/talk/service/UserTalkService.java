package io.jpress.module.talk.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Columns;
import io.jpress.module.talk.model.UserTalk;

import java.util.List;

public interface UserTalkService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public UserTalk findById(Object id);


    /**
     * find all model
     *
     * @return all <UserTalk
     */
    public List<UserTalk> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(UserTalk model);


    /**
     * save model to database
     *
     * @param model
     * @return  id value if save success
     */
    public Object save(UserTalk model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if saveOrUpdate success
     */
    public Object saveOrUpdate(UserTalk model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(UserTalk model);


    /**
     * paginate query
     *
     * @param page
     * @param pageSize
     * @return
     */
    public Page<UserTalk> paginate(int page, int pageSize);


    Page<UserTalk> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);
}
