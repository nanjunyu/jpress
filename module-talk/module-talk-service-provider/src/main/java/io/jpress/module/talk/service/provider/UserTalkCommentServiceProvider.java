package io.jpress.module.talk.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jpress.module.talk.service.UserTalkCommentService;
import io.jpress.module.talk.model.UserTalkComment;
import io.jboot.service.JbootServiceBase;

@Bean
public class UserTalkCommentServiceProvider extends JbootServiceBase<UserTalkComment> implements UserTalkCommentService {

}