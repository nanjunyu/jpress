package io.jpress.module.talk.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jpress.module.talk.service.UserTalkService;
import io.jpress.module.talk.model.UserTalk;
import io.jboot.service.JbootServiceBase;

@Bean
public class UserTalkServiceProvider extends JbootServiceBase<UserTalk> implements UserTalkService {

}