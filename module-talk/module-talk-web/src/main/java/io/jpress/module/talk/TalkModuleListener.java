package io.jpress.module.talk;

import com.jfinal.core.Controller;
import io.jpress.core.menu.MenuGroup;
import io.jpress.core.module.ModuleListener;

import java.util.List;

/**
 * @Author Frank
 * @Date Create in  2019/4/29 16:00
 */
public class TalkModuleListener implements ModuleListener {
    @Override
    public String onRenderDashboardBox(Controller controller) {
        return null;
    }

    @Override
    public String onRenderToolsBox(Controller controller) {
        return null;
    }

    @Override
    public void onConfigAdminMenu(List<MenuGroup> adminMenus) {

        MenuGroup menuGroup = new MenuGroup();
        menuGroup.setId("photo");
        menuGroup.setText("说说");
        menuGroup.setIcon("<i class=\"fa fa-photo\"></i>");
        menuGroup.setOrder(1);

        adminMenus.add(menuGroup);
    }

    @Override
    public void onConfigUcenterMenu(List<MenuGroup> ucenterMenus) {

    }
}
