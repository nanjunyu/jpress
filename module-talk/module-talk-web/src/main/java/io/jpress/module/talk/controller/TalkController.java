package io.jpress.module.talk.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.jfinal.upload.UploadFile;
import io.jboot.utils.FileUtil;
import io.jboot.web.controller.JbootControllerContext;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.JPressOptions;
import io.jpress.commons.utils.AliyunOssUtils;
import io.jpress.commons.utils.PhotoUtils;
import io.jpress.model.Attachment;
import io.jpress.module.talk.model.UserTalk;
import io.jpress.module.talk.service.UserTalkService;
import io.jpress.service.AttachmentService;
import io.jpress.web.base.TemplateControllerBase;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author Frank
 * @Date Create in  2019/4/30 13:59
 */
@RequestMapping("/talk")
public class TalkController extends TemplateControllerBase {
    @Inject
    UserTalkService userTalkService;

    @Inject
    private AttachmentService service;

    public void index() {

        render("page_talk_list.html");
    }


    public void postTalkUpload() {
        HttpServletRequest request = JbootControllerContext.get().getRequest();
        if (getLoginedUser() == null) {
            renderJson(Ret.fail().set("message", "未登录用户不能上传图片").set("errorCode", 9));
            return;
        }

        if (!isMultipartRequest()) {
            renderError(404);
            return;
        }

        List<UploadFile> uploadFileList = getFiles();
        if (uploadFileList.isEmpty()) {
            renderJson(Ret.fail().set("message", "请选择要上传的文件"));
            return;
        }
        List<String> list = new ArrayList<>();
        for (UploadFile uploadFile : uploadFileList) {
            File file = uploadFile.getFile();
            if (PhotoUtils.isUnSafe(file)) {
                file.delete();
                renderJson(Ret.fail().set("message", "不支持此类文件上传"));
                return;
            }

            String mineType = uploadFile.getContentType();
            String fileType = mineType.split("/")[0];
            Integer maxImgSize = JPressOptions.getAsInt("attachment_img_maxsize", 2);
            Integer maxOtherSize = JPressOptions.getAsInt("attachment_other_maxsize", 100);
            Integer maxSize = "image".equals(fileType) ? maxImgSize : maxOtherSize;

            int fileSize = Math.round(file.length() / 1024 * 100) / 100;
            if (fileSize > maxSize * 1024) {
                file.delete();
                renderJson(Ret.fail().set("message", "上传文件大小不能超过 " + maxSize + " MB"));
                return;
            }

            String path = PhotoUtils.moveFile(uploadFile,"talk");
            AliyunOssUtils.upload(path, PhotoUtils.file(path));

            Attachment attachment = new Attachment();
            attachment.setUserId(getLoginedUser().getId());
            attachment.setTitle(uploadFile.getOriginalFileName());
            attachment.setPath(path.replace("\\", "/"));
            attachment.setSuffix(FileUtil.getSuffix(uploadFile.getFileName()));
            attachment.setMimeType(uploadFile.getContentType());

            service.save(attachment);
          /*  StringBuffer url = request.getRequestURL();
            String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();*/
            list.add(attachment.getPath());
        }

        renderJson(Ret.ok().set("success", true).set("list", list));
    }

    public void talkPost() {
        UserTalk userTalk = new UserTalk();
        String content = getPara("content");
        userTalk.setContent(content);
        userTalk.setCreated(new Date());
        userTalk.setStatus("1");
        if (getLoginedUser() == null) {
            renderJson(Ret.fail().set("message", "未登录用户不能发表说说").set("errorCode", 9));
            return;
        }
        userTalk.setUserId(getLoginedUser().getId());
        userTalkService.save(userTalk);
        renderJson(Ret.ok().set("success", true));

    }

    public void talkLike() {
        UserTalk userTalk = new UserTalk();
        String content = getPara("content");
        userTalk.setContent(content);
        userTalk.setCreated(new Date());
        userTalk.setStatus("1");
        if (getLoginedUser() == null) {
            renderJson(Ret.fail().set("message", "未登录用户不能发表说说").set("errorCode", 9));
            return;
        }
        userTalk.setUserId(getLoginedUser().getId());
        userTalkService.save(userTalk);
        renderJson(Ret.ok().set("success", true));

    }
}
