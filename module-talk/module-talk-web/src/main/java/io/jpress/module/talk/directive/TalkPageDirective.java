
package io.jpress.module.talk.directive;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.db.model.Columns;
import io.jboot.web.controller.JbootControllerContext;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import io.jboot.web.directive.base.PaginateDirectiveBase;
import io.jpress.JPressOptions;
import io.jpress.model.User;
import io.jpress.module.article.directive.DirectveKit;
import io.jpress.module.article.model.Article;
import io.jpress.module.article.service.ArticleService;
import io.jpress.module.talk.model.UserTalk;
import io.jpress.module.talk.service.UserTalkCommentService;
import io.jpress.module.talk.service.UserTalkService;
import io.jpress.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michael Yang 杨福海 （fuhai999@gmail.com）
 * @version V1.0
 * @Package io.jpress.module.page.directive
 */
@JFinalDirective("talkPage")
public class TalkPageDirective extends JbootDirectiveBase {

    @Inject
    private UserTalkService userTalkService;

    @Inject
    UserTalkCommentService userTalkCommentService;

    @Inject
    ArticleService articleService;

    @Inject
    UserService userService;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        Controller controller = JbootControllerContext.get();

        int page = controller.getParaToInt(1, 1);
        int pageSize = getParaToInt("pageSize", scope, 10);
        int normalCount = articleService.findCountByStatus(Article.STATUS_NORMAL);
        Columns columns = Columns.create("status", 1);
        Page<UserTalk> userTalkPage = userTalkService.paginateByColumns(page, pageSize,columns,"created desc");
        if (userTalkPage != null && userTalkPage.getTotalRow() > 0) {
            for (UserTalk userTalk : userTalkPage.getList()) {
                Map<String,Object> map=new HashMap<>(16);
                Long userId = userTalk.getUserId();
                User user = userService.findById(userId);
                map.put("nickName",user.getNickname());
                map.put("headImg",user.getAvatar());
                userTalk.put("nickName",user.getNickname());
                userTalk.put("headImg",user.getAvatar());
            }
        }
        scope.setGlobal("commentSize", userTalkCommentService.findAll().size());
        scope.setGlobal("talkPageSize", userTalkPage.getTotalRow());
        scope.setGlobal("normalCount", normalCount);
        scope.setGlobal("talkPage", userTalkPage);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
/*
    @JFinalDirective("talkPaginate")
    public static class TemplatePaginateDirective extends PaginateDirectiveBase {
        private boolean firstGotoIndex = false;

        @Override
        public void onRender(Env env, Scope scope, Writer writer) {
            firstGotoIndex = getPara("firstGotoIndex", scope, false);
            super.onRender(env, scope, writer);
        }


        @Override
        protected String getUrl(int pageNumber) {
            HttpServletRequest request = JbootControllerContext.get().getRequest();
            String url = request.getRequestURI();
            String contextPath = JFinal.me().getContextPath();

            if (pageNumber == 1 && firstGotoIndex) {
                return contextPath + "/";
            }

            // 如果当前页面是首页的话
            // 需要改变url的值，因为 上一页或下一页是通过当前的url解析出来的
            if (url.equals(contextPath + "/")) {
                url = contextPath + "/talk/category/index"
                        + JPressOptions.getAppUrlSuffix();
            }
            return DirectveKit.replacePageNumber(url, pageNumber);
        }

        @Override
        protected Page<?> getPage(Env env, Scope scope, Writer writer) {
            return (Page<?>) scope.get("talkPage");
        }
    }*/

}
