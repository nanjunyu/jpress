
package io.jpress.module.talk.directive;

import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.controller.JbootControllerContext;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.PaginateDirectiveBase;
import io.jpress.JPressOptions;
import io.jpress.module.article.directive.DirectveKit;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Michael Yang 杨福海 （fuhai999@gmail.com）
 * @version V1.0
 * @Package io.jpress.module.page.directive
 */
@JFinalDirective("talkPaginate")
public class TalkPageInateDirective extends PaginateDirectiveBase {
        private boolean firstGotoIndex = false;

        @Override
        public void onRender(Env env, Scope scope, Writer writer) {
            firstGotoIndex = getPara("firstGotoIndex", scope, false);
            super.onRender(env, scope, writer);
        }


        @Override
        protected String getUrl(int pageNumber) {
            HttpServletRequest request = JbootControllerContext.get().getRequest();
            String url = request.getRequestURI();
            if(url.equals("/talk")){
                url="/talk/";
            }
            String contextPath = JFinal.me().getContextPath();

            if (pageNumber == 1 && firstGotoIndex) {
                return contextPath + "/";
            }

            // 如果当前页面是首页的话
            // 需要改变url的值，因为 上一页或下一页是通过当前的url解析出来的
            if (url.equals(contextPath + "/")) {
                url = contextPath + "/talk/category/index"
                        + JPressOptions.getAppUrlSuffix();
            }
            return DirectveKit.replacePageNumber(url, pageNumber);
        }

        @Override
        protected Page<?> getPage(Env env, Scope scope, Writer writer) {
            return (Page<?>) scope.get("talkPage");
        }
    }

